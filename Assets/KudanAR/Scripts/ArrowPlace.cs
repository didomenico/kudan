﻿using UnityEngine;
using System.Collections;

namespace Kudan.AR
{
	public class ArrowPlace : MonoBehaviour 
	{
		public TrackingMethodMarkerless markerless;
		public KudanTracker tracker;
		public GameObject[] activeObjs;
		public GameObject arrow;

		void Awake()
		{
			activeObjs = GameObject.FindGameObjectsWithTag ("Markerless");
		}

		void Update()
		{
			Vector3 pos;
			Quaternion rot;

			arrow.gameObject.SetActive (false);
			
			for (int i = 0; i < activeObjs.Length; i++)
			{
				if (tracker.CurrentTrackingMethod == markerless && !activeObjs[i].activeInHierarchy) 
				{
					arrow.gameObject.SetActive (true);
				}
			}

			if (arrow.gameObject.activeSelf) 
			{
				tracker.FloorPlaceGetPose (out pos, out rot);

				if (pos.z < 0) 
				{
					pos.Set (pos.x, pos.y, (pos.z * -1));
					arrow.transform.position = pos;
					arrow.transform.Translate (0, -25, 0);
					arrow.transform.rotation = rot;
				} 
				else 
				{
					arrow.transform.position = pos;
					arrow.transform.Translate (0, -25, 0);
					arrow.transform.rotation = rot;
					arrow.transform.Rotate (180, 0, 0);
				}
			}
		}
	}
}