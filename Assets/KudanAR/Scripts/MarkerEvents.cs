﻿using UnityEngine;
using System.Collections;

namespace Kudan.AR
{
	public class MarkerEvents : MonoBehaviour 
	{
		public TrackingMethodMarker markT;
		public MarkerTransformDriver markTran;
		Trackable[] trackables;

		// Struct for tracking marker events
		// Each marker has its own separate name and bools so they can be differentiated
		public struct markerStruct
		{
			public GameObject marker;	// The marker that has been detected.

			public string name;			// The name of the detected marker. This is the name of the object containing the Marker Transform Driver script in the Hieracrhy view.

			public bool isActive;		// Bools check the current status of the marker
			public bool wasActive;		// and whether or not it was active in the previous frame
		}

		// An array of structs that allows each marker to be individually checked.
		markerStruct[] markers;

		// An array of GameObjects to store the different markers that are active in the current frame.
		// These get passed into the "marker" GameObject of each element in the the markerStruct array.
		GameObject[] markerArray;

		public int numMaxEventTracking = 10; // The maximum number of markers being tracked at any one time. 10 is the default to keep things simple. Would you need to track more than 10 markers at a time? I doubt it.

		void Start ()
		{
			// Initialise the array
			markers = new markerStruct[numMaxEventTracking];
			trackables = new Trackable[numMaxEventTracking];
		}

		void Update ()
		{
			// GameObject.FindGameObjectsWithTag automatically resizes an array to be the same size as the number of objects it finds,
			// so initialising it in start is not necessary.
			markerArray = GameObject.FindGameObjectsWithTag ("Marker");
			trackables = markT.Plugin.GetDetectedTrackablesAsArray ();

			if (markerArray.Length < numMaxEventTracking)
			{
				// But it does mean that it must be resized here, otherwise searcching through it gives an "Out of range" exception,
				// especially if there are currently no markers active.
				Resize (numMaxEventTracking, ref markerArray);
			}

			for (int i = 0; i < numMaxEventTracking; i++)
			{
				// Both arrays are now the same size, so each element of markerArray is assigned to the marker GameObject of the markers array
				markers [i].marker = markerArray [i];

				for (int j = 0; j < trackables.Length; j++)
				{
					if(markTran._expectedId.Contains(trackables[j].name))
					{
						markers[i].name = trackables[j].name;
					}
				}

				if (markers [i].marker == null) 
				{
					// If the marker is null (There is no marker), then it isn't active, so set isActive to false,
					// then check whether or not the marker was lost.
					markers [i].isActive = false;
					checkLost (markers[i]);
					markers [i].wasActive = false;
				} 

				else 
				{
					// If the marker is not null, it is active, so set isActive to true,
					// then check whether or not the marker was found and if it is currently being tracked.
					markers [i].isActive = true;
					checkFound (markers[i]);
					markers [i].wasActive = true;
					checkTrack (markers[i]);
				}
			}
		}

		// The Resize function simply takes a number to resize to, initialises a temporary array of that size,
		// copies all elements of the referenced array to the temporary array and finally assigns the temporary array
		// to the referenced array. This prevents any "Out of range" exception errors.
		void Resize(int size, ref GameObject[] array)
		{
			GameObject[] temp = new GameObject[size];
			for (int i = 0; i < Mathf.Min (size, array.Length); i++) 
			{
				temp [i] = array [i];
			}

			array = temp;
		}

		public void checkFound(markerStruct marker)	// If the marker was not active last frame but is active this frame, this must be the frame in which the marker was detected
		{
			if (marker.isActive && !marker.wasActive)
			{
				//name = marker.name;
				Debug.LogWarning ("Found Marker: " + marker.name);
			}
		}

		public void checkTrack(markerStruct marker)	
		{
			if (marker.marker.activeInHierarchy) {
				Debug.LogWarning ("Tracking Marker: " + marker.name);	// If the marker is active, then it is being tracked each frame.
			} else
				marker.marker = null;									// If the marker is not active, set it to null so the array knows it has been lost.
		}

		public void checkLost(markerStruct marker)	// If the marker was active last frame but isn't active this frame, this must be the frame in which the marker was lost.
		{
			if (!marker.isActive && marker.wasActive)
			{
				Debug.LogWarning ("Lost Marker: " + marker.name);
			}
		}
	}
}